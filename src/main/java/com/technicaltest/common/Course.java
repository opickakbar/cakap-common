package com.technicaltest.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonDeserialize(builder = Course.CourseBuilder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Course implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("course_name")
    private String courseName;

    @JsonProperty("author")
    private String author;

    @JsonProperty("description")
    private String description;

    @JsonProperty("url")
    private String url;

    @Builder
    public Course(String courseName, String author, String description, String url) {
        this.courseName = courseName;
        this.author = author;
        this.description = description;
        this.url = url;
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CourseBuilder {

        @JsonProperty("course_name")
        public CourseBuilder courseName(String courseName) {
            this.courseName = courseName;
            return this;
        }

        @JsonProperty("author")
        public CourseBuilder author(String author) {
            this.author = author;
            return this;
        }

        @JsonProperty("description")
        public CourseBuilder description(String description) {
            this.description = description;
            return this;
        }

        @JsonProperty("url")
        public CourseBuilder url(String url) {
            this.url = url;
            return this;
        }
    }
}
