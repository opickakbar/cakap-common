package com.technicaltest.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class TestResponse implements Serializable {

    @JsonProperty("response_code")
    private String responseCode;

    @Builder
    public TestResponse(String responseCode) {
        this.responseCode = responseCode;
    }
}
