package com.technicaltest.common.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonException extends RuntimeException {
    private static final long serialVersionUID = 4223361764991527999L;
    public CommonException(String message) {
        super(message);
    }
}
