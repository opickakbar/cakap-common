package com.technicaltest.common.request.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonDeserialize(builder = LoginAndRegisterRequest.LoginAndRegisterRequestBuilder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginAndRegisterRequest implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    private String password;

    @Builder
    public LoginAndRegisterRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LoginAndRegisterRequestBuilder {

        @JsonProperty("username")
        public LoginAndRegisterRequestBuilder username(String username) {
            this.username = username;
            return this;
        }

        @JsonProperty("password")
        public LoginAndRegisterRequestBuilder password(String password) {
            this.password = password;
            return this;
        }
    }
}
