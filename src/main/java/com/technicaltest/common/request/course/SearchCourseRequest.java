package com.technicaltest.common.request.course;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonDeserialize(builder = SearchCourseRequest.SearchCourseRequestBuilder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchCourseRequest implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("search")
    private String search;

    @Builder
    public SearchCourseRequest(String search) {
        this.search = search;
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SearchCourseRequestBuilder {

        @JsonProperty("search")
        public SearchCourseRequestBuilder search(String search) {
            this.search = search;
            return this;
        }
    }
}
