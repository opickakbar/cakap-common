package com.technicaltest.common.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class ConfigResponse implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("key")
    private String key;

    @JsonProperty("value")
    private String value;

    @Builder
    public ConfigResponse(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
