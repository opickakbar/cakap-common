package com.technicaltest.common.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("response_message")
    private String responseMessage;

    @Builder
    public ErrorResponse(Boolean status, String responseMessage) {
        this.status = status;
        this.responseMessage = responseMessage;
    }
}
