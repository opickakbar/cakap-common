package com.technicaltest.common.response.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonDeserialize(builder = CheckTokenResponse.CheckTokenResponseBuilder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckTokenResponse implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("username")
    private String username;

    @JsonProperty("role")
    private String role;

    @Builder
    public CheckTokenResponse(Boolean status, String message, String username, String role) {
        this.status = status;
        this.message = message;
        this.username = username;
        this.role = role;
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CheckTokenResponseBuilder {

        @JsonProperty("status")
        public CheckTokenResponseBuilder status(Boolean status) {
            this.status = status;
            return this;
        }

        @JsonProperty("message")
        public CheckTokenResponseBuilder message(String message) {
            this.message = message;
            return this;
        }

        @JsonProperty("username")
        public CheckTokenResponseBuilder username(String username) {
            this.username = username;
            return this;
        }

        @JsonProperty("role")
        public CheckTokenResponseBuilder role(String role) {
            this.role = role;
            return this;
        }
    }
}
