package com.technicaltest.common.response.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class LoginResponse implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("token")
    private String token;

    @Builder
    public LoginResponse(Boolean status, String token) {
        this.status = status;
        this.token = token;
    }
}
