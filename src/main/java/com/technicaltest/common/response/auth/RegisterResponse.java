package com.technicaltest.common.response.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class RegisterResponse implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("message")
    private String message;

    @Builder
    public RegisterResponse(Boolean status, String message) {
        this.status = status;
        this.message = message;
    }
}
