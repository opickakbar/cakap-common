package com.technicaltest.common.response.course;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@EqualsAndHashCode
public class InsertCourseResponse implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("message")
    private String message;

    @Builder
    public InsertCourseResponse(Boolean status, String message) {
        this.status = status;
        this.message = message;
    }
}
