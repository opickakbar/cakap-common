package com.technicaltest.common.response.course;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.technicaltest.common.Course;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class SearchCourseResponse implements Serializable {

    private static final long serialVersionUID = 12132L;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    private List<Course> data;

    @Builder
    public SearchCourseResponse(Boolean status, String message, List<Course> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
