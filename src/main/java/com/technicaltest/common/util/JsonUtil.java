package com.technicaltest.common.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class JsonUtil {

    private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);

    public JsonUtil() {
    }

    public static JsonNode toJsonNode(String jsonString) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readTree(jsonString);
        } catch (Exception var2) {
            log.error("Failed to convert string to jsonNode, error = {}", var2.getMessage());
            return null;
        }
    }

    public static <T> String toJsonString(T object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(object);
        } catch (Exception var2) {
            log.error("Failed to convert object to string, error = {}", var2.getMessage());
            return null;
        }
    }

    public static Map<String, String> toMap(String jsonString) {
        HashMap dataMap = null;
        try {
            JsonParser parser = new JsonParser();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            Type type = (new TypeToken<HashMap<String, String>>() {
            }).getType();
            JsonObject jsonObject = (JsonObject) parser.parse(jsonString);
            dataMap = (HashMap) gson.fromJson(jsonObject, type);
        } catch (Exception var7) {
            log.error("Failed to convert string to map, error = {}", var7.getMessage());
        }
        return dataMap;
    }

    public static <T> T toObject(String jsonString, Class<T> cls) {
        Object var2 = null;

        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
            Gson gson = gsonBuilder.create();
            return gson.fromJson(jsonString, cls);
        } catch (Exception var5) {
            log.error("Failed to convert string to object, error = {}", var5.getMessage());
            return null;
        }
    }
}
